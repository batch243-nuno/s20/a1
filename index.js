let countDown = function countDown(number) {
    console.log(`The number you provided is ${number}`);
    for (i = 0; number >= i; number -= 5) {
        if(number % 10 === 0) {
        console.log("The number is divisible by 10. Skipping the number");
        continue;
        }
        else if(number <= 50) {
            console.log("The current value is at 50. Terminating the loop");
            break;
        }
        console.log(number); 
    }
  };
//   let input = Number(prompt("Enter a number"));
  let input = 100;
  countDown(input);
  
  let newWord = "";
  let removeVowel = function removeVowel(word) {
    console.log(word);
    for (index = 0; index < word.length; index++) {
      if (word[index] === "a" || word[index] === "e" || word[index] === "i" || word[index] === "o" || word[index] === "u") {
        continue;
      }
      else {
        newWord += word[index];
      }
    }
    console.log(newWord);
  };
  let word = "supercalifragilisticexpialidocious";
  removeVowel(word);
